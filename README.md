# Simon Mansfield's Personal README

## Purpose
The idea of this README is inspired by other GitLab team members who have shared personal READMEs, as well as Sid's transparency about his [own flaws](https://about.gitlab.com/handbook/ceo/#flaws).  Hopefully, this document will serve to be an honest portrait of who I am and how I work.  And it is intentionally in a public repository so that if I'm wrong or someone knows me better than I do, they can create a merge request to fix it.

## Personal Stuff

I have a beautiful wife and two wonderful little daughters (born Nov 4th 2019 and August 3rd 2023). We live just outside the village of [Street](https://en.wikipedia.org/wiki/Street,_Somerset) and town, [Glastonbury](https://www.countrylife.co.uk/out-and-about/glastonbury-somerset-the-place-where-the-holy-grail-came-to-britain-251783) (famous for it's [music festival](https://i2-prod.somersetlive.co.uk/incoming/article7247783.ece/ALTERNATES/s1227b/1_what-a-sight-am-846886.jpg) - the largest greenfield music and performing arts festival in the world).

Having two young kids can make early morning and late afternoon/early evening calls (UK timezone) hard to accomodate, as I'm likely getting my daughters up, fed, bathed and finally put to bed! Given the fact I travel a fair bit for work and can sometimes be away for extended periods, this time with my family is absolutely critical to me - so expect a refusal to any meetings that take this away from me, without first giving advance notice! (I work to live, not live to work!)

## Personality / Emotional Intelligence

### Meyer's Briggs

Having done these tests fairly regularly over the years, I often "flip" between two different results, so I've included them both for reference! (Strategy always remains the same however!)

- **Personality:** 
  - 👨‍💼 Executive [ESTJ-A](https://www.16personalities.com/estj-personality) (Extraverted, Observant, Thinking, Judging, Assertive) - Executives are excellent organizers, unsurpassed at managing things – or people. They possess fortitude, emphatically following their sensible judgment and offering solid direction.
  - 👨‍✈️ Commander [ENTJ-A](https://www.16personalities.com/entj-personality) (Extraverted, Intuitive, Thinking, Judging, Assertive) - Commanders are bold, imaginative, and strong-willed, always finding a way – or making one. These decisive types love momentum and accomplishment, often acting on their creative visions.
- **Role:**
  - 🛡️ [Sentinel](https://www.16personalities.com/articles/roles-sentinels) - Sentinels are cooperative and practical, embracing and creating order, security, and stability. They’re hard-working, meticulous, and traditional, preferring to stick to their plans.
  - 📖 [Analyst](https://www.16personalities.com/articles/roles-analysts) - Analysts embrace rationality and impartiality, excelling in intellectual debates and scientific or technological fields. They are fiercely independent, open-minded, and strong-willed.
- **Strategy:** 🤝 [People Mastery](https://www.16personalities.com/articles/strategies-people-mastery) - Individuals who prefer the People Mastery Strategy seek social contact and have very good communication skills, feeling at ease in social events or directing others. They’re confident and readily express their opinions.

If you're interested in reading more, you can find my current full readout [here](https://www.16personalities.com/profiles/54c1d0566cb89).

### My "Tilt"

The tilt assessment I feel is slightly less 'specific' than the results above, though I consider it useful, so have shared here as well.

**Connection - Cross Pollinator:** Tilting in CONNECTION means I am focused on connecting PEOPLE and IDEAS and have a natural intuition for reading other people's emotions. I know how to lean into their needs to give them precisely what they appear to want.

This comes from doing a Tilt 365 assessment, more info [available here](https://www.credly.com/badges/109e6ff1-16bd-4794-b1a0-467b2b667eeb)

## Work

### Home Office Setup

So here's my home office equipment list, though for anyone wondering almost all of it was self-purchased (GitLab helped out with a couple of items):

* LG 38" 144Hz Ultrawide (lower)
* Samsung 34" 60Hz Ultrawide (upper)
* Duronic Vertical Monitor Arm
* Elgato Wave Mic Arm LP (Low Profile)
* RODE Procaster Microphone
* RODE Shock Mount
* Scarlett Solo XLR Sound Interface
* Topping D90 USB DAC
* Topping A90 Headphone Amplifier
* LOTS of headphones - Sennheiser HD598SE, Sennheiser HD6XX, AKG K712, Beyerdynamic DT990 Pro, etc. (I used to review headphones as a side hobby!)
* KRK ROKIT 5 G4 Powered Monitors (Speakers)
* Omoton MacBook Pro Stand
* Apple Wireless TouchID Keyboard
* Logitech PRO Superlight Mouse
* Hermon Miller Aeron Chair
* Canon EOS Mirrorless Camera + 22mm Lens
* Elgato Key Light (x2)
* Elgato Green Screen (out of sight!)

## This document
* I am not perfect, neither is this document!
* This document is as much for me as it is for you.
* I expect that if I'm not acting by the ideals stated here, you'll constructively point that out to me.
* I expect that if you do that, I will say thank you!  If I don't - send me the link to this document.
* If something needs to change about this document, please submit a merge request to it.
